package library;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static library.ExtentReporting.extent_Pass;
import static library.ExtentReporting.extent_Warn;

public class Utilities {
    static Long defaultTimeout = Long.valueOf(30);
    public static void loadurl(WebDriver driver,String url) throws InterruptedException {
        Logg.logger.info("url:"+url);
        driver.get(url);
        //driver.navigate().to(url);
        waitForPageLoaded(driver);
    }
    public static WebDriver createDriver(String browserName) {
        Logg.logger.info("browserName:" + browserName);
        WebDriver driver = null;
        try {
            switch (browserName.toLowerCase()) {
                case "ie":
                    System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "/src/main/resources/IEDriverServer.exe");
                    InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
                    internetExplorerOptions.ignoreZoomSettings();
                    //internetExplorerOptions.destructivelyEnsureCleanSession();
                    internetExplorerOptions.enablePersistentHovering();
                    //internetExplorerOptions.disableNativeEvents();
                    internetExplorerOptions.setCapability("locationContextEnabled", "true");
                    driver = new InternetExplorerDriver(internetExplorerOptions);
                    break;

                case "chrome":

                    Map<String, Object> prefs = new HashMap<String, Object>();
                    prefs.put("credentials_enable_service", false);
                    prefs.put("password_manager_enabled", false);

                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("prefs", prefs);
                    options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
                    options.addArguments("start-maximized");

                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe");
                    driver = new ChromeDriver(options);
                    break;

                case "firefox":

                    System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/main/resources/geckodriver.exe");
                    driver = new FirefoxDriver();

                    break;

                default:
                    driver.manage().window().maximize();
                    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            }
            return driver;
        }
        catch (Exception e)
        {
            Logg.logger.info("Failed to create webdriver:"+browserName);
            throw e;
        }
    }
    public static void waitForPageLoaded(WebDriver driver) {
        Logg.logger.info("");
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        WebDriverWait wait = new WebDriverWait(driver,30);
        try {
            wait.until(expectation);
            extent_Pass(driver,"The page is loaded successfully ", driver.getTitle(), "", "");
        } catch(Throwable error) {
            extent_Warn(driver,"Timeout waiting for Page Load Request to complete.", driver.getTitle(), "", "");
        }
    }
    public static WebElement waitAndClick(WebDriver driver, WebElement element)
    {
        Logg.logger.info("element:" + element);
        try{
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.elementToBeClickable(element));
            scrollToElement(driver,element);
            extent_Pass(driver, "Scrolled on element successfully",element.toString(),"","");
            element.click();
            extent_Pass(driver, "Clicked on element successfully",element.toString(),"","");
            //waitUntilElementIsNotVisible(driver,element);
            return element;
        }
        catch(Exception e)
        {
            Logg.logger.info("Exception occured in waitAndClick: "+e.toString());
            extent_Warn(driver,"Failed to click on element",element.toString(),"","");
            throw  e;
        }

    }
    public static WebElement waitAndSendKeys(WebDriver driver, WebElement element, String text) {
        Logg.logger.info(String.format("element:%s, text:%s" , element,text));
        try{
            waitUntilElementIsVisible(driver,element);
            element.clear();
            element.sendKeys(text);
            extent_Pass(driver,"Entered the text",text," to element:"+element,"");
            return element;
        }
        catch(Exception e)
        {
            Logg.logger.info("Exception occured in waitAndSendKeys: "+e.getMessage());
            extent_Warn(driver,"Failed to enter text",text," on "+element,"");
            e.printStackTrace();
            throw  e;
        }
    }

    public static WebElement scrollToElement(WebDriver driver, WebElement element)
    {
        Logg.logger.info("element:"+element);
        try {
            ((JavascriptExecutor) driver).executeScript(
                    "arguments[0].scrollIntoView(true); ", element);
            JavascriptExecutor jse = (JavascriptExecutor)driver;
            jse.executeScript("scroll(10, 0)");
            return element;
        }catch (Exception e)
        {
            Logg.logger.info("Exception occured in ScrollToElement:"+e.toString());
            extent_Warn(driver,"Could not scroll to the element",element.toString(),"",e.getMessage());
            throw e;
        }
    }
    public static WebElement waitUntilElementIsVisible(WebDriver driver,WebElement element)
    {
        Logg.logger.info("Element:"+element);
        try {
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.visibilityOf(element));
            return scrollToElement(driver,element);
        }
        catch(Exception e)
        {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: ");
            extent_Warn(driver,"Could not find the element",element.toString(),"",e.getMessage());
            e.printStackTrace();
            throw  e;
        }
    }
    public static String takeScreenshot(WebDriver driver)
    {
        Logg.logger.info("");
        try {
            String fileName = new SimpleDateFormat("yyyyMMddmmssSSSZ").format(new Date());
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String path = System.getProperty("user.dir") + "/test-output/Screenshots/"+ fileName+".jpeg";
            FileUtils.copyFile(screenshot, new File(path));
            return "Screenshots/"+fileName+".jpeg";
        } catch (Exception e) {
            Logg.logger.info("Failed to take screenshot : " + e.getMessage());
            extent_Warn(driver,"Failed to take screenshot","","","");
            e.printStackTrace();
        }
        return null;
    }
}
