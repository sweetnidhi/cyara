package library;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.javafx.binding.StringFormatter;
import org.openqa.selenium.WebDriver;

import java.io.File;

public class ExtentReporting {
    public static ExtentTest extentTest;
    public static ExtentReports extentReport = null;
    protected static boolean addPassScreenshot = true;
    protected static String reportFilePath=null;
    public static ExtentReports getExtentReport()
    {
        return extentReport;
    }
    public static boolean getAddPassScreenshot() {
        return addPassScreenshot;
    }
    public static String getReportFilePath() {
        return reportFilePath;
    }

    public static void setReportFilePath(String reportFilePath) {
        ExtentReporting.reportFilePath = reportFilePath;
    }

    public static void setAddPassScreenshot(boolean addPassScreenshot) {
        ExtentReporting.addPassScreenshot = addPassScreenshot;
    }
    public static ExtentReports configureExtentReport(String reportName, boolean addPassScreenshot1,String url) {
        if(extentReport == null ) {
            Logg.logger.info(StringFormatter.format(" reportName: %s", reportName));
            setReportFilePath(System.getProperty("user.dir") + "\\test-output\\" + reportName);
            extentReport = new ExtentReports(reportFilePath);
            extentReport.addSystemInfo("Test Environment",url);
            extentReport.loadConfig(new File(System.getProperty("user.dir") + "//configs//extentReportConfig.xml"));
            setAddPassScreenshot(addPassScreenshot1);
        }
        return extentReport;
    }    /**
     * Function to catch the extent_Reports PASSED exception
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Pass(WebDriver driver, String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s",step_description,input_value,expected_value,actual_value));
        try {
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value=(input_value==""?"":" | Input_value:"+input_value);
            expected_value=(expected_value==""?"":" | Expected_value:"+expected_value);
            actual_value=(actual_value==""?"":" | Actual_value:"+actual_value);
            extentTest.log(LogStatus.PASS,step_description+input_value+ expected_value + actual_value);
            if(addPassScreenshot) extentTest.log(LogStatus.PASS, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured: " +e.getMessage());
            extentTest.log(LogStatus.INFO,e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }
    /**
     * Function to catch the extent_Reports PASSED exception
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Pass_NoScreenshot(WebDriver driver, String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s",step_description,input_value,expected_value,actual_value));
        try {
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value=(input_value==""?"":" | Input_value:"+input_value);
            expected_value=(expected_value==""?"":" | Expected_value:"+expected_value);
            actual_value=(actual_value==""?"":" | Actual_value:"+actual_value);
            extentTest.log(LogStatus.PASS,step_description+input_value+ expected_value + actual_value);
            //if(addPassScreenshot) extentTest.log(LogStatus.PASS, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured: " +e.getMessage());
            extentTest.log(LogStatus.INFO,e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Function to catch the extent_Reports Warning exception
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Warn(WebDriver driver, String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s",step_description,input_value,expected_value,actual_value));
        try {
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.WARNING, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value=(input_value==""?"":" | Input_value:"+input_value);
            expected_value=(expected_value==""?"":" | Expected_value:"+expected_value);
            actual_value=(actual_value==""?"":" | Actual_value:"+actual_value);
            extentTest.log(LogStatus.WARNING,step_description+input_value+expected_value+actual_value);
            extentTest.log(LogStatus.WARNING, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured: " +e.getMessage());
            extentTest.log(LogStatus.INFO,e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Function to catch extent_Reports FAILURE exception
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Failure(WebDriver driver, String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s", step_description, input_value, expected_value, actual_value));
        try {
            //screenshot();
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value = (input_value == "" ? "" : " | Input_value:" + input_value);
            expected_value = (expected_value == "" ? "" : " | Expected_value:" + expected_value);
            actual_value = (actual_value == "" ? "" : " | Actual_value:" + actual_value);
            extentTest.log(LogStatus.FAIL, step_description + input_value + expected_value + actual_value);
            extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured; " + e.getMessage());
            extentTest.log(LogStatus.INFO, e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

//    /**
//     * Function to set Author information in ATU Report for each script
//     * @param author
//     * @param version
//     */
//    public static void extent_SetAuthorInfoForReports(String author, String version) {
//        Logg.logger.info(String.format("author:%s, version:%s",author,version));
//        ATUReports.setAuthorInfo(author, Utils.getCurrentTime(),version);
//    }
//
//
        /**
         * Function to set Test case requirement coverage in ATU Report for each script
         * @param testCaseDesc
         */
        public static void extent_SetTestCaseRegCoverageForReports(String testCaseDesc) {
            Logg.logger.info("TestCaseDesc:"+ testCaseDesc);
            //ATUReports.setTestCaseReqCoverage(TestCaseDesc);
            extentTest.log(LogStatus.INFO,testCaseDesc);
        }


        /**
         * Function to set ATU Report index html customisation in ATU Report
         */
//    public static void extent_Report_Customise() {
//        Logg.logger.info("");
//        ATUReports.setWebDriver(driver);
//        ATUReports.indexPageDescription = "<strong>BC1  Automation Test Report</strong>. <br /> Click on Consolidated Reports to view the full summary report.";
//        //Functions.extent_SetAuthorInfoForReports("Neema","1.0");
//    }

        
}

