package library;
import org.apache.log4j.*;

public class Logg {
    public static PatternLayout layout;
    public static ConsoleAppender consoleAppender;
    public static FileAppender fileAppender = null;
    public static Logger rootLogger;
    public static Logger logger;

    public static void configure() {
        if(fileAppender == null) {
            layout = new PatternLayout();
            //String conversionPattern = "%-7p %d [%t] %c %x - %m%n";
            String conversionPattern = "%d{dd MMM yyyy HH:mm:ss} %C{1}.%M:%L -- %m%n";
            layout.setConversionPattern(conversionPattern);
            // creates console appender
            consoleAppender = new ConsoleAppender();
            consoleAppender.setLayout(layout);
            consoleAppender.activateOptions();
            consoleAppender.setThreshold(Priority.INFO);
            BasicConfigurator.configure(consoleAppender);

            // creates file appender
            fileAppender = new FileAppender();
            fileAppender.setFile("log.txt");
            fileAppender.setLayout(layout);
            fileAppender.setThreshold(Priority.INFO);
            fileAppender.activateOptions();
            BasicConfigurator.configure(fileAppender);

//        // configures the root logger
//        rootLogger = Logger.getRootLogger();
//        rootLogger.setLevel(Level.DEBUG);
//        rootLogger.addAppender(consoleAppender);
//        rootLogger.addAppender(fileAppender);

            // creates a custom logger and log messages
            logger = Logger.getLogger(Logg.class);
            logger.debug("this is a debug log message");
            logger.info("this is a information log message");
            logger.warn("this is a warning log message");
        }
    }
}
