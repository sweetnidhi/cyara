package pageObjects;

import library.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static library.Utilities.waitAndClick;
import static library.Utilities.waitUntilElementIsVisible;

public class TokenPage extends BasePageClass{

    public TokenPage(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(css = "a[data-logged-in-user-name]")
    private WebElement userDropdown;
    @FindBy(xpath="//a[contains(text(),'API')]")
    private WebElement APIButton;
    @FindBy(id="generate")
    private WebElement generateTokenButton;
    @FindBy(css="div.well.well-small.monospace")
    private WebElement token;
    public String generateToken()
    {
        Logg.logger.info("");
        waitAndClick(driver,userDropdown);
        waitAndClick(driver,APIButton);
        waitAndClick(driver,generateTokenButton);
        return getGeneratedToken();
    }
    public String getGeneratedToken()
    {
        Logg.logger.info("");
        waitUntilElementIsVisible(driver,token);
        String full= token.getText();
        String arr[]=full.split(" ");
        return arr[1]+" "+arr[2];
    }

}
