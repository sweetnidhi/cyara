package pageObjects;

import library.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static library.Utilities.waitAndClick;
import static library.Utilities.waitAndSendKeys;

public class LoginPage extends BasePageClass {
    public LoginPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(id="Username")
    private WebElement userName;
    @FindBy(id="Password")
    private WebElement password;
    @FindBy(id="loginButton")
    private WebElement loginButton;

    public void login(String user,String pass)
    {
        Logg.logger.info("User name: "+user+" Password: "+pass);
        waitAndSendKeys(driver,userName,user);
        waitAndSendKeys(driver,password,pass);
        waitAndClick(driver,loginButton);
    }
}
