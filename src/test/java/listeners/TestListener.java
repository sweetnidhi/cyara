package listeners;

import com.relevantcodes.extentreports.LogStatus;
import library.Logg;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import static library.ExtentReporting.*;

public class TestListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult iTestResult) {
        Logg.logger.info("**** Test: "+iTestResult.getMethod().getMethodName()+" started ****");
        extentTest = extentReport.startTest(iTestResult.getTestClass().getName()+"."+iTestResult.getName());
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        Logg.logger.info("**** Test: "+iTestResult.getMethod().getMethodName()+" PASSED ****");
        extentTest.log(LogStatus.PASS,"Test "+iTestResult.getMethod().getMethodName()+" passed");
        extentReport.endTest(extentTest);
        extentReport.flush();
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        Logg.logger.info("**** Test: "+iTestResult.getMethod().getMethodName()+" FAILED ****");
        extentTest.log(LogStatus.FAIL,"Test "+iTestResult.getMethod().getMethodName()+" failed");
        extentReport.endTest(extentTest);
        extentReport.flush();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Logg.logger.info("**** Test: "+iTestResult.getMethod().getMethodName()+" SKIPPED ****");
        extentTest.log(LogStatus.SKIP,"Test "+iTestResult.getMethod().getMethodName()+" failed");
        extentReport.endTest(extentTest);
        extentReport.flush();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        Logg.logger.info("");
        extentTest = extentReport.startTest("Test: "+iTestContext.getName());
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        Logg.logger.info("");
       // EmailReporter.createEmailReport(getReportFilePath());
    }
}
