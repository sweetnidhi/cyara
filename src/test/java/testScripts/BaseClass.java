package testScripts;

import library.ExtentReporting;
import library.Logg;
import library.Utilities;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pageObjects.LoginPage;
import pageObjects.TokenPage;

import java.util.Date;

public class BaseClass {
    public WebDriver chromeDriver;
    public WebDriver ieDriver;
    public LoginPage loginPage;
    public TokenPage tokenPage;
    public static long StartTime = new Date().getTime();
    public static long defaultTimeout;
    public static String reportName="ExtentReportResults.html";

    @BeforeSuite
    @Parameters ( { "addPassScreenshot" , "url" })
    public void beforeSuite(boolean addScreenshotIfStepPass,String url) {
        Logg.configure();
        Logg.logger.info("");
        StartTime = new Date().getTime();
        defaultTimeout = 20;
        ExtentReporting.configureExtentReport(reportName,addScreenshotIfStepPass,url);
    }
    @BeforeTest
    public void beforeTest()
    {
        chromeDriver = Utilities.createDriver("chrome");
        loginPage = new LoginPage(chromeDriver);
        tokenPage = new TokenPage(chromeDriver);
    }
    @AfterTest
    public void afterTest()
    {
        Logg.logger.info("");
        chromeDriver.quit();
    }
}
