package testScripts;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import library.Logg;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;

import static com.jayway.restassured.RestAssured.given;
import static library.Utilities.loadurl;
import org.testng.annotations.*;

public class CyaraTestScripts extends BaseClass{
    private String token;
    private String apiUrl;
    private String userId="37";
    @BeforeClass
    @Parameters({"url","api_url"})
    public void beforeClass(String url, String apiUrl) throws InterruptedException, IOException, AWTException {
        Logg.logger.info("URL:"+url);
        loadurl(chromeDriver, url);
        loginPage.login("test.user2","Pass@word123");
        token=tokenPage.generateToken();
        Logg.logger.info("Token:"+token);
        this.apiUrl = apiUrl;
    }
    @Test
    public void postAndGetServiceTest()
    {
        Logg.logger.info("");
        String file = System.getProperty("user.dir") + "/resourceFiles/post_Service.json";
        try{
            //Post the service using the file
            FileInputStream f = new FileInputStream(file);
            byte b[] = new byte[1000];
            f.read(b);
            String req= new String(b);
            Response resp = given().
                    header("Authorization",token).
                    contentType(ContentType.JSON).
                    body(req.trim()).
                    when().
                    post(apiUrl+"/v3.0/accounts/"+userId+"/services");
            JSONArray arr = new JSONArray("[" + resp.asString() + "]");
            JSONObject jobt =  arr.getJSONObject(0);
            Logg.logger.info(jobt);
            Assert.assertEquals(resp.statusCode(),200);
            String serviceId= resp.then().
                    contentType(ContentType.JSON).
                    extract().
                    path("serviceId");
            Logg.logger.info("Service Id created:"+serviceId);
            //Get and verify that service exists
            Response resp1 = given().header("Authorization",token).
                    when().
                    get(apiUrl+"/v3.0/accounts/"+userId+"/services/"+serviceId);
            Logg.logger.info(resp1.getStatusCode());
            arr = new JSONArray("[" + resp1.asString() + "]");
            jobt =  arr.getJSONObject(0);
            Logg.logger.info(jobt);
            Assert.assertEquals(resp1.getStatusCode(),200);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    @Test
    public void getFailureServiceTest()
    {
        Logg.logger.info("");
            //Get an invalid service Id that does not exist
            Response resp1 = given().header("Authorization",token).
                    when().
                    get(apiUrl+"/v3.0/accounts/"+userId+"/services/Invalid");
            JSONArray arr = new JSONArray("[" + resp1.asString() + "]");
            JSONObject jobt =  arr.getJSONObject(0);
            Logg.logger.info(jobt);
            Assert.assertEquals(resp1.getStatusCode(),400);
    }
    @Test
    public void postFailureTest()
    {
        Logg.logger.info("");
        String file = System.getProperty("user.dir") + "/resourceFiles/post_fail.json";
        try {
            //Post the service using the file
            FileInputStream f = new FileInputStream(file);
            byte b[] = new byte[1000];
            f.read(b);
            String req = new String(b);
            Response resp = given().
                    header("Authorization", token).
                    contentType(ContentType.JSON).
                    body(req.trim()).
                    when().
                    post(apiUrl + "/v3.0/accounts/" + userId + "/services");
            JSONArray arr = new JSONArray("[" + resp.asString() + "]");
            JSONObject jobt =  arr.getJSONObject(0);
            Logg.logger.info(jobt);
            Assert.assertEquals(resp.statusCode(), 400);
        } catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    @Test
    public void deleteServiceTest()
    {
        Logg.logger.info("");
        String file = System.getProperty("user.dir") + "/resourceFiles/post_Service.json";
        try {
            //Post the service using the file
            FileInputStream f = new FileInputStream(file);
            byte b[] = new byte[1000];
            f.read(b);
            String req = new String(b);
            Response resp = given().
                    header("Authorization", token).
                    contentType(ContentType.JSON).
                    body(req.trim()).
                    when().
                    post(apiUrl + "/v3.0/accounts/" + userId + "/services");
            JSONArray arr = new JSONArray("[" + resp.asString() + "]");
            JSONObject jobt =  arr.getJSONObject(0);
            Logg.logger.info(jobt);
            Assert.assertEquals(resp.statusCode(), 200);
            String serviceId = resp.then().
                    contentType(ContentType.JSON).
                    extract().
                    path("serviceId");
            Logg.logger.info("Service Id created:" + serviceId);

            //Get and verify that service exists
            Response resp1 = given().header("Authorization",token).
                    when().
                    get(apiUrl+"/v3.0/accounts/"+userId+"/services/"+serviceId);
            Logg.logger.info(resp1.getStatusCode());
            arr = new JSONArray("[" + resp1.asString() + "]");
            jobt =  arr.getJSONObject(0);
            Logg.logger.info(jobt);
            Assert.assertEquals(resp1.getStatusCode(),200);

            //Delete the posted service
            given().
                    header("Authorization", token).
                    contentType(ContentType.JSON).
                    param("ids", serviceId).
                    when().
                    delete(apiUrl + "/v3.0/accounts/"+userId+"/services")
                    .then()
                    .statusCode(200);

            //Get and verify that the service does not exist and is deleted
            resp = given().header("Authorization", token).
                    when().
                    get(apiUrl+"/v3.0/accounts/"+userId+"/services/"+serviceId);
            Logg.logger.info(resp.getStatusCode());
            Assert.assertEquals(resp.getStatusCode(), 404);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    @Test
    public void deleteServiceFailureTest()
    {
        Logg.logger.info("");
            //Delete the service that does not exist
            Response resp = given().
                    header("Authorization", token).
                    contentType(ContentType.JSON).
                    param("ids", "Invalid").
                    when().
                    delete(apiUrl + "/v3.0/accounts/"+userId+"/services");
        JSONArray arr = new JSONArray("[" + resp.asString() + "]");
        JSONObject jobt =  arr.getJSONObject(0);
        Logg.logger.info(jobt);
        Assert.assertEquals(resp.getStatusCode(),200);

    }
    @Test
    public void putServiceTest() {
        Logg.logger.info("");
        String put_file = System.getProperty("user.dir") + "/resourceFiles/put_Service.json";
        try{
            String post_file = System.getProperty("user.dir") + "/resourceFiles/post_Service.json";
            //Post the service using the file
            FileInputStream f = new FileInputStream(post_file);
            byte b[] = new byte[1000];
            f.read(b);
            String req = new String(b);
            Response resp = given().
                    header("Authorization", token).
                    contentType(ContentType.JSON).
                    body(req.trim()).
                    when().
                    post(apiUrl + "/v3.0/accounts/" + userId + "/services");
            Assert.assertEquals(resp.statusCode(), 200);
            String serviceId = resp.then().
                    contentType(ContentType.JSON).
                    extract().
                    path("serviceId");
            Logg.logger.info("Service Id created:" + serviceId);

            //Get and verify that service exists
            Response resp1 = given().header("Authorization",token).
                    when().
                    get(apiUrl+"/v3.0/accounts/"+userId+"/services/"+serviceId);
            Logg.logger.info(resp1.getStatusCode());
            JSONArray arr = new JSONArray("[" + resp1.asString() + "]");
            JSONObject jobt =  arr.getJSONObject(0);
            Logg.logger.info(jobt);
            Assert.assertEquals(resp1.getStatusCode(),200);
            //Put request using a new file - name is changed to Bye
            f = new FileInputStream(put_file);
            byte b1[] = new byte[1000];
            f.read(b1);
            req = new String(b1);
            resp = given().
                    header("Authorization",token).
                    contentType(ContentType.JSON).
                    body(req.trim()).
                    when().
                    put(apiUrl+"/v3.0/accounts/"+userId+"/services/"+serviceId);
            Assert.assertEquals(resp.statusCode(),200);
            String actualName = resp.then().
                    contentType(ContentType.JSON).
                    extract().
                    path("name");
            Assert.assertEquals("Bye",actualName);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    @Test
    public void putServiceFailureTest() {
        Logg.logger.info("");
        String put_file = System.getProperty("user.dir") + "/resourceFiles/put_Service.json";
        try {
            //Put request using a new file - name is changed to Bye
            FileInputStream f = new FileInputStream(put_file);
            byte b1[] = new byte[1000];
            f.read(b1);
            String req = new String(b1);
            Response resp = given().
                    header("Authorization", token).
                    contentType(ContentType.JSON).
                    body(req.trim()).
                    when().
                    put(apiUrl + "/v3.0/accounts/" + userId + "/services/Invalid");
            Assert.assertEquals(resp.statusCode(), 400);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
