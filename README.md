This is a simple project to work with RestAssured to test some REST APIs.

Prerequisites:
1.	Download and install Java from here if u don’t have it
2.	Download Github from here and install with default settings
3.	Download Maven from here and unzip it in any folder (preferably within C:\Program Files). Installation instruction also here
4.	Make sure JAVA_HOME variable is created and updated to the jdk path. Add the maven bin folder path to the PATH variable.
Steps:
1.	Clone our repository from git (open console and goto the folder where you want to checkout. Give command ‘git clone’)
2.	Goto the Cyara Folder (cd Cyara)
3.	Give the command ‘mvn clean test’ this will automatically trigger all tests in testng.xml 
4.	If u want to run using Intellij, start Intellij and open the cyara project.
5.	Right click on Project->Module Settings and select the JDK there. To run tests, open the Testng and right click and Run
